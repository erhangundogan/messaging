import { useState, useEffect } from 'react';
import axios from 'axios';

const author = process.env.REACT_APP_AUTHOR;
const token = process.env.REACT_APP_DOODLE_API_TOKEN;
const url = process.env.REACT_APP_DOODLE_API_URL;

const useMessages = () => {
  const [messages, setMessages] = useState([]);
  const [timestamp, setTimestamp] = useState(null);

  const setTimeAndMessages = (data) => {
    setMessages((prevMessages) => [...prevMessages, ...data]);
    setTimestamp(Date.now());
  };

  const getMessages = async () => {
    const { data } = await axios.get(url, {
      params: {
        token,
        ...(timestamp ? { since: timestamp } : {}),
      },
    });
    setTimeAndMessages(data);
  };

  const sendMessage = async (message) => {
    const { data } = await axios.post(url,
      { author, message },
      {
        headers: {
          'Content-Type': 'application/json',
          token,
        }
      });
    setTimeAndMessages(Array.isArray(data) ? data : [data]);
  };

  useEffect(() => {
    getMessages();
  }, []);

  return {
    messages,
    sendMessage,
  };
};

export default useMessages;
