import axios from 'axios';
import { renderHook } from '@testing-library/react-hooks';
import useMessages from '../useMessages';

jest.mock('axios');

describe('useMessages hook', () => {
  const timestamp = 1596381902257;
  const mockMessage = {
    author: 'John Smith',
    message: 'Hello world',
    token: 'token',
    timestamp,
    _id: timestamp,
  };

  beforeEach(() => {
    axios.get.mockResolvedValue({ data: [mockMessage] });
    axios.post.mockResolvedValue({ data: mockMessage });
  });

  test('loads messages', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useMessages());

    await waitForNextUpdate();

    expect(result.current.messages).toEqual([mockMessage]);
  });

  test('sends message', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useMessages());

    await waitForNextUpdate();

    expect(result.current.messages.length).toBe(1);

    result.current.sendMessage(mockMessage);

    await waitForNextUpdate();

    expect(result.current.messages.length).toBe(2);
  });
});
