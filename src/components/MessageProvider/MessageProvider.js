import React from 'react';
import useMessages from '../../hooks/useMessages';
import MessageContext from '../../contexts/MessageContext';

const MessageProvider = ({ children }) => {
  const { messages, sendMessage } = useMessages();

  return <MessageContext.Provider value={{ messages, sendMessage }}>{children}</MessageContext.Provider>;
};

export default MessageProvider;
