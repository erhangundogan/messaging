import React, { useContext, useEffect } from 'react';
import MessageContext from '../../contexts/MessageContext';
import { MessageView, TextBox } from '../index';
import './App.css';

const App = () => {
  const { messages, sendMessage } = useContext(MessageContext);

  useEffect(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, [messages]);

  return (
    <div className="app-container">
      <div className="content">
        <MessageView messages={messages} />
      </div>
      <TextBox sendMessage={sendMessage} />
    </div>
  );
};

export default App;
