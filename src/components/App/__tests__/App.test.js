import React from 'react';
import axios from 'axios';
import { render, wait } from '@testing-library/react';
import App from '../App';
import MessageProvider from '../../MessageProvider/MessageProvider';

jest.mock('axios');

describe('<App />', () => {
  const timestamp = 1596381902257;

  global.scrollTo = jest.fn();

  const tree = () =>
    render(
      <MessageProvider>
        <App />
      </MessageProvider>
    );

  test('renders properly', async () => {
    axios.get.mockResolvedValue({
      data: [
        {
          author: 'John Smith',
          message: 'Hello world',
          token: 'token',
          timestamp,
          _id: timestamp,
        },
      ],
    });
    const { getByText } = tree();

    await wait(() => {
      expect(getByText('John Smith')).toBeInTheDocument();
      expect(getByText('Hello world')).toBeInTheDocument();
    });
  });
});
