import React from 'react';
import dateFormat from 'dateformat';
import './Message.css';

const Message = ({ author, message, timestamp, isMyMessage = author === process.env.REACT_APP_AUTHOR }) => (
  <div className={`message-container ${isMyMessage ? 'my-message-container' : ''}`}>
    <div className={`message-wrapper ${isMyMessage ? 'my-message-wrapper' : ''}`}>
      {!isMyMessage && <div className="secondary-text">{author}</div>}
      <div className="primary-text">{message}</div>
      <div className="secondary-text date-text">{dateFormat(+timestamp, 'dS mmm yyyy HH:MM')}</div>
    </div>
  </div>
);

export default Message;
