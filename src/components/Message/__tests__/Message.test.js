import React from 'react';
import { render } from '@testing-library/react';
import Message from '../Message';

describe('<Message />', () => {
  const OLD_ENV = process.env;
  const timestamp = 1596381902257;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...OLD_ENV };
  });

  const tree = ({ author, message, timestamp }) =>
    render(<Message author={author} message={message} timestamp={timestamp} />);

  test('render others message', () => {
    process.env.REACT_APP_AUTHOR = 'Michael Jackson';

    const props = {
      author: 'John Smith',
      message: 'Hello world',
      timestamp,
    };

    const { asFragment } = tree(props);
    expect(asFragment()).toMatchSnapshot();
  });

  test('render own message', () => {
    process.env.REACT_APP_AUTHOR = 'John Smith';

    const props = {
      author: 'John Smith',
      message: 'Hello world',
      timestamp,
    };

    const { asFragment } = tree(props);
    expect(asFragment()).toMatchSnapshot();
  });
});
