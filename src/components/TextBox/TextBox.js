import React from 'react';
import './TextBox.css';

const TextBox = ({ sendMessage }) => {
  const onSubmit = (event) => {
    event.preventDefault();

    const {
      message: { value },
    } = event.target.elements;

    if (value) {
      sendMessage(value);
      event.currentTarget.reset();
    }
  };

  return (
    <div className="text-box-container">
      <div className="text-box-layout">
        <form className="text-box-form" onSubmit={onSubmit}>
          <input className="text-box-input" name="message" type="text" placeholder="Message" />
          <input className="text-box-send" type="submit" value="Send" />
        </form>
      </div>
    </div>
  );
};

export default TextBox;
