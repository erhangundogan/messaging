import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import TextBox from '../TextBox';

describe('<TextBox />', () => {
  const tree = (sendMessage) => render(<TextBox sendMessage={sendMessage} />);

  test('renders component', () => {
    const { asFragment } = tree();
    expect(asFragment()).toMatchSnapshot();
  });

  test('calls submit method', () => {
    const sendMessage = jest.fn();

    const { getByPlaceholderText, getByDisplayValue } = tree(sendMessage);
    const messageInput = getByPlaceholderText('Message');
    const submitInput = getByDisplayValue('Send');

    expect(messageInput).toBeInTheDocument();
    expect(submitInput).toBeInTheDocument();

    fireEvent.change(messageInput, { target: { value: 'This is my message!' } });
    fireEvent.click(submitInput);

    expect(sendMessage).toHaveBeenCalledTimes(1);
    expect(sendMessage).toHaveBeenCalledWith('This is my message!');
  });
});
