import React from 'react';
import { Message } from '../index';

const MessageView = ({ messages }) => (
  <div className="message-view">
    {messages.map(({ _id, author, message, timestamp }) => (
      <Message key={_id} {...{ author, message, timestamp }} />
    ))}
  </div>
);

export default MessageView;
