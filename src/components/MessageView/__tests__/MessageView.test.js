import React from 'react';
import { render } from '@testing-library/react';
import MessageView from '../MessageView';
import mockMessages from '../../../hooks/data.json';

describe('<MessageView />', () => {
  const tree = (messages) => render(<MessageView messages={messages} />);

  test('renders empty view', () => {
    const { asFragment } = tree([]);
    expect(asFragment()).toMatchSnapshot();
  });

  test('renders messages array', () => {
    const { asFragment } = tree(mockMessages);
    expect(asFragment()).toMatchSnapshot();
  });
});
