import React from 'react';
import ReactDOM from 'react-dom';
import { App, MessageProvider } from './components';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <MessageProvider>
      <App />
    </MessageProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
