Doodle Messaging F/E
====================

Doodle messaging F/E challenge. 

Technology stack for the application:

- Create-react-app
- React 16+
- React hooks
- Axios
- NodeJS v12.14.1 or later

# Setup

- Provide environment variables from .env file (copy .env.example to .env and fill author, token)
- Use yarn/npm to install dependencies.
 
```bash
yarn
```
```
npm i
```


# Run

Run application on http://localhost:3000

```bash
yarn start
```
```bash
npm start
```

# Test

Run unit tests with Jest.

```bash
yarn test
```

# Lint

ESLint command below should work. You can also add `--fix` to automatically fix errors.

```bash
yarn lint
```

#### Browser support

async/await is part of ECMAScript 2017 and is not supported in Internet Explorer and older browsers.

For older browsers fetch + polyfill library could be used.

#### Notes

First time I have used reducer for the messages but then I changed it to custom hook. Chat interface with sticky footer was challenging. Typescript could be used for type safety. There could be sync intervals for getting messages as well but I didn't add it.
